if (
    /^http:\/\/rx-git\/root\/xact\/-\/merge_requests\/?(\?.*)?$/.test(
        window.location.href
    ) || /^https:\/\/gitlab\.*.*\/merge_requests\/?(\?.*)?$/.test(
        window.location.href)
) {
    function createThreadsBadge(element, color, resolved, resolvable, newAnswers, url) {
        const li = $("<li/>")
            .addClass("issuable-comments d-none d-sm-flex")
            .prependTo(element);
        $("<span/>")
            .addClass("badge color-label")
            .css("background-color", color)
            .css("color", "#333333")
            .html(`${newAnswers != 0 ? '✉ ' : ''}<a href="${url}"> ${resolved}/${resolvable} thread${resolvable != 1 ? 's' : ''}  resolved</a>`)
            .prependTo(li);
    }

    $(".merge-request").each(function () {
        const anchor = $(this).find(".merge-request-title-text a")[0];
        const metaList = $(this).find(".issuable-meta ul")[0];

        $.ajax({
            url: `${anchor.href}/discussions.json`,
            success: function (result) {
                let resolvable = 0;
                let resolved = 0;
                let newAnswers = 0;
                let username = document.getElementsByClassName("current-user")[0].querySelector("a").dataset.user;
                result.forEach(item => {
                    if (item.notes[0].author.username == username) {
                        if (item.resolvable) resolvable++;
                        if (item.resolved) resolved++;
                        if (item.resolvable && !item.resolved && item.notes[item.notes.length - 1].author.username != username)
                            newAnswers++;
                    }
                });

                if (resolvable > resolved) {
                    createThreadsBadge(metaList, "#fca14b", resolved, resolvable, newAnswers, anchor.href);
                } else if (resolved === resolvable && resolvable > 0) {
                    createThreadsBadge(metaList, "#a4f980", resolved, resolvable, newAnswers, anchor.href);
                }
            }
        });
    });
}